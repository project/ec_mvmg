$(document).ready(function(){
  $("TR").filter(".details").hide();
  $("TR").filter(".showing").show();

  // show hide links on the txn summary page for shoppers
  $("a").filter(".showhide").click( function() { 
    $(this).parents("TR").next().toggle();
    return false;
  } );
});
